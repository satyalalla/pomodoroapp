# pomodoroApp
### Patrick Blair, Satyavrath Injamuri, Jyoshna Boppidi, Sanjana Tummala
### Overview: 
This app assists in improving time management skills by employing the Pomodoro Technique, which makes use of work intervals (i.e. 25 minutes by default), separated by short breaks (i.e. 5 minutes). After this process has been repeated a certain number of times, the user gets a longer break (i.e. 30 minutes). After their longer break, the user restarts the process. This app allows the user to customize work/break intervals, number of repetitions, etc. 
