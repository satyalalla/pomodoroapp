//
//  Timer.swift
//  pomoapp
//
//  Created by We are Go-getters on 4/4/19.
//  Copyright © 2019 We are Go-getters. All rights reserved.
//

import Foundation

class PomoTimers {
    static let shared = PomoTimers()
    var selectedTimer:[TimeInterval] = []
    var selectedTimerName:String?
    var timeIntervals:[PomoTimer] = [PomoTimer(timerName: "Default", workLength: 1, shortBreakLength: 1, longBreakLength: 1, repetitionsBeforeLongBreak: 2, totalRepetitions: 1)]
    
    // Deletes a timer
    func delete(at:Int) {
        timeIntervals.remove(at: at)
    }
    
    // Returns number of PomoTimers
    func numTimers() -> Int {
        return timeIntervals.count
    }
    
    // Adds a PomoTimer
    func add(pomoTimer:PomoTimer) {
        timeIntervals.append(pomoTimer)
    }
    
    // Returns a PomoTimer at a specific index
    subscript(index:Int) -> PomoTimer {
        return timeIntervals[index]
    }
}

class PomoTimer {
    
    var timerName:String
    var workLength:Int
    var shortBreakLength:Int
    var longBreakLength:Int
    var repititionsBeforeLongBreak:Int
    var totalRepititions:Int
    
    init(timerName:String, workLength:Int, shortBreakLength:Int, longBreakLength:Int, repetitionsBeforeLongBreak:Int, totalRepetitions:Int) {
        self.timerName = timerName
        // Convert to seconds
        self.workLength = workLength * 60
        self.shortBreakLength = shortBreakLength * 60
        self.longBreakLength = longBreakLength * 60
        
        self.repititionsBeforeLongBreak = repetitionsBeforeLongBreak
        self.totalRepititions = totalRepetitions
    }
    
    func genTimeStack() -> [TimeInterval] {
        var timeStack:[TimeInterval] = []
        for _ in 1...totalRepititions {
            for _ in 1...repititionsBeforeLongBreak {
                timeStack.append(TimeInterval(timeSeconds: workLength, intervalType: "Work!"))
                timeStack.append(TimeInterval(timeSeconds: shortBreakLength, intervalType: "Short break!"))
            }
            // Remove last index - we don't want a short break before a long break
            timeStack.remove(at: timeStack.count - 1)
            timeStack.append(TimeInterval(timeSeconds: longBreakLength, intervalType: "Long break!"))
        }
        // Remove last index - we don't want a break after the user is done working - they are done
        timeStack.remove(at: timeStack.count - 1)
        // Reverse timeStack so it can function "like" a stack.
        timeStack.reverse()
        return timeStack
    }
}

class TimeInterval {
    // Represents an interval of time
    var timeSeconds:Int
    var intervalType:String
    
    init(timeSeconds:Int, intervalType:String) {
        self.timeSeconds = timeSeconds
        self.intervalType = intervalType
    }
}
