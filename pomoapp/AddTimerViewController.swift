//
//  AddTimerViewController.swift
//  pomoapp
//
//  Created by We are Go-getters on 4/6/19.
//  Copyright © 2019 We are Go-getters. All rights reserved.
//

import UIKit

class AddTimerViewController: UIViewController {
    let pomoTimers = PomoTimers.shared
    
    @IBOutlet weak var timerNameTF: UITextField!
    @IBOutlet weak var workLengthTF: UITextField!
    @IBOutlet weak var shortBreakTF: UITextField!
    @IBOutlet weak var longBreakTF: UITextField!
    @IBOutlet weak var repititionsBeforeLongBreakTF: UITextField!
    @IBOutlet weak var totalRepititionsTF: UITextField!
    @IBOutlet weak var errorLBL: UILabel!
    
    // Exit, do not save data
    @IBAction func cancelBTN(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Save data and exit
    @IBAction func doneBTN(_ sender: Any) {
        // Validate correct input
        if timerNameTF.text == nil || Int(workLengthTF.text!) == nil || Int(shortBreakTF.text!) == nil || Int(longBreakTF.text!) == nil || Int(repititionsBeforeLongBreakTF.text!) == nil || Int(totalRepititionsTF.text!) == nil {
            errorLBL.text = "Invalid input - integer values please"
        }
        else {
            // If data is formatted correctly, save and exit
            pomoTimers.add(pomoTimer: PomoTimer(timerName: timerNameTF.text!, workLength: Int(workLengthTF.text!)!, shortBreakLength: Int(shortBreakTF.text!)!, longBreakLength: Int(longBreakTF.text!)!, repetitionsBeforeLongBreak: Int(repititionsBeforeLongBreakTF.text!)!, totalRepetitions: Int(totalRepititionsTF.text!)!))
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
