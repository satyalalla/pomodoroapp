
//
//  TimerViewController.swift
//  pomoapp
//
//  Created by We are Go-getters on 4/6/19.
//  Copyright © 2019 We are Go-getters. All rights reserved.
//

import UIKit
import AVFoundation

class TimerViewController: UIViewController {
    var av=AVAudioPlayer()
    let pomoTimers = PomoTimers.shared
    var timeIntervals:[TimeInterval] = []
    
    @IBOutlet weak var timerNameLBL: UILabel!
    @IBOutlet weak var controlBTN: UIButton!
    @IBOutlet weak var selOrCancelTimerBTN: UIButton!
    @IBOutlet weak var intervalLBL: UILabel!
    @IBOutlet weak var timerLBL: UILabel!

    @IBOutlet weak var timerView: TimerView!
    var currentTime = 1.0
    var totalTime = 1.0
    
    // The timer
    var timer:Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let sound = Bundle.main.path(forResource: "ding", ofType: "mp3")
        do {
            // set up audio player
            av = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
        } catch {
            print(error)
        }
    }
    
    // Starts the selected timer
    @IBAction func controlBTN(_ sender: Any) {
        timeIntervals = pomoTimers.selectedTimer
        if controlBTN.titleLabel?.text! == "Start" {
            // Start the timer
            if timeIntervals.count > 0 {
                // Set up timer label
                timerNameLBL.text = "\(pomoTimers.selectedTimerName!)"
                // Change button text styles to reflect current options
                controlBTN.setTitle("Pause", for: .normal)
                selOrCancelTimerBTN.setTitle("Cancel", for: .normal)
                // Set interval label to the current interval
                intervalLBL.text = timeIntervals[timeIntervals.count - 1].intervalType
                
                // Create the actual timer. This executes the code in the fireTimer function once every second
                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)
                
                // Set up animation and play sound
                currentTime = 0.0
                totalTime = Double(pomoTimers.selectedTimer[pomoTimers.selectedTimer.count-1].timeSeconds)
                av.play()
            } else {
                // No timer configuration selected
                intervalLBL.text = "Please select a timer!"
            }
        } else if controlBTN.titleLabel?.text! == "Pause" {
            // Pause (invalidate) the timer
            controlBTN.setTitle("Resume", for: .normal)
            timer.invalidate()
        } else if controlBTN.titleLabel?.text! == "Resume" {
            // Start the timer again
            controlBTN.setTitle("Pause", for: .normal)
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)
        }
    }
    
    // Select a timer configuration, or cancel current timer
    @IBAction func selOrCancelTimerBTN(_ sender: Any) {
        if selOrCancelTimerBTN.titleLabel?.text! == "Cancel" {
            // Then timer is currently running - reset it and relevant labels/buttons
            timer.invalidate()
            timerLBL.text = "00:00"
            selOrCancelTimerBTN.setTitle("Select timer", for:.normal)
            pomoTimers.selectedTimer = []
            intervalLBL.text = ""
            timerNameLBL.text = ""
            controlBTN.setTitle("Start", for: .normal)
            timerView.reDraw(currentTime: 1, totalTime: 1)
        } else {
            // Timer is not currently running, segue to SelectTimerTableViewController
            self.performSegue(withIdentifier: "viewTimersSegue", sender: self)
        }
    }
    
    // This code will execute every second that the timer is running
    @objc func fireTimer() {
        // If timeIntervals is empty, invalidate timer
        if timeIntervals.count == 0 {
            timer.invalidate()
        } else {
            timeIntervals[timeIntervals.count - 1].timeSeconds -= 1
            // Format the timer label to display time in a more readbale format than sconds
            timerLBL.text = String(format: "%02d:%02d", timeIntervals[timeIntervals.count - 1].timeSeconds / 60, timeIntervals[timeIntervals.count - 1].timeSeconds % 60)
            // Remove last element if the timer has reached the correct duration
            if timeIntervals[timeIntervals.count - 1].timeSeconds <= 0 {
                // The end of a time interval has been reaching
                timeIntervals.removeLast()
                if timeIntervals.count > 0 {
                    intervalLBL.text = timeIntervals[timeIntervals.count - 1].intervalType
                    if timeIntervals[timeIntervals.count - 1].intervalType == "Work!" {
                        // User is working
                        av.play()
                        currentTime = 0.0
                        totalTime = Double(timeIntervals[timeIntervals.count - 1].timeSeconds)
                    } else if timeIntervals[timeIntervals.count - 1].intervalType == "Short break!" || timeIntervals[timeIntervals.count - 1].intervalType == "Long break!"{
                        // User is in a break
                        av.play()
                        totalTime = Double(timeIntervals[timeIntervals.count - 1].timeSeconds)
                        currentTime = totalTime
                    }
                } else {
                    // Then user is done.
                    // Play sound and set label accordingly
                    av.play()
                    intervalLBL.text = "Done!"
                }
            }
            // Animate timer in accordance with current time
            if timeIntervals.count > 0 && timeIntervals[timeIntervals.count - 1].intervalType == "Work!" {
                currentTime += 1.0
                timerView.reDraw(currentTime: currentTime, totalTime: totalTime)
            } else if timeIntervals.count > 0 && (timeIntervals[timeIntervals.count - 1].intervalType == "Short break!" || timeIntervals[timeIntervals.count - 1].intervalType == "Long break!") {
                timerView.reDraw(currentTime: currentTime, totalTime: totalTime)
                currentTime -= 1.0
            }
        }        
    }
}


class TimerView: UIView {
    var currentTime = 1.0
    var totalTime = 1.0
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        let fillAngle = CGFloat.pi * CGFloat(currentTime/totalTime)
        let remaining = UIBezierPath(arcCenter: CGPoint(x: self.bounds.maxX/2, y: self.bounds.maxY/2), radius: CGFloat(bounds.maxX/2-10), startAngle: CGFloat.pi/2 - fillAngle, endAngle: CGFloat.pi/2 + fillAngle, clockwise: false)
        UIColor.green.setFill()
        remaining.close()
        remaining.fill()
        let fillPath = UIBezierPath(arcCenter: CGPoint(x: self.bounds.maxX/2, y: self.bounds.maxY/2), radius: CGFloat(bounds.maxX/2-10), startAngle: CGFloat.pi/2 - fillAngle, endAngle: CGFloat.pi/2 + fillAngle, clockwise: true)
        UIColor.red.setFill()
        fillPath.close()
        fillPath.fill()
    }
    // this function notify the system that view's need to be redrawn
    func reDraw(currentTime:Double, totalTime:Double){
        self.currentTime = currentTime
        self.totalTime = totalTime
        setNeedsDisplay()
    }
}
