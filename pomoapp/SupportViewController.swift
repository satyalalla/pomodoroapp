//
//  SupportViewController.swift
//  pomoapp
//
//  Created by We are Go-getters on 4/3/19.
//  Copyright © 2019 We are Go-getters. All rights reserved.
//

import UIKit
import MessageUI

class SupportViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // this function enables to link to facebook page
    @IBAction func PomoFB(_ sender: Any) {
        let fbURLWeb = NSURL(string: "https://www.facebook.com/pomoapp.ios.37")
        let fbURLID = NSURL(string: "fb://profile/100035201002185")
        if(UIApplication.shared.canOpenURL(fbURLID! as URL)){
            UIApplication.shared.canOpenURL(fbURLID! as URL)
        } else {
            UIApplication.shared.open(fbURLWeb! as URL, options: [:], completionHandler: nil)
        }
        
    }
    // this function enables to link to twitter page
    @IBAction func PomoTwitter(_ sender: Any) {
        let twitterURLWeb = NSURL(string: "https://twitter.com/AppPomo")
        let twitterURLID = NSURL(string: "twitter://profile/2443083368")
        if(UIApplication.shared.canOpenURL(twitterURLID! as URL)){
            UIApplication.shared.canOpenURL(twitterURLID! as URL)
        } else {
            UIApplication.shared.open(twitterURLWeb! as URL, options: [:], completionHandler: nil)
        }
        
    }
    // this function enables to link to Instagram page
    @IBAction func PomoInsta(_ sender: Any) {
        let instagramURLWeb = NSURL(string: "https://www.instagram.com/pomoapp/")
        let instagramURLID = NSURL(string: "instagram://profile/12332371592")
        if(UIApplication.shared.canOpenURL(instagramURLID! as URL)){
            UIApplication.shared.canOpenURL(instagramURLID! as URL)
        } else {
            UIApplication.shared.open(instagramURLWeb! as URL, options: [:], completionHandler: nil)
        }
    }
    // this function enables to send email from mobile application
    @IBAction func Support(_ sender: Any) {
        let mailComposeViewController = configureMailController()
        if MFMailComposeViewController.canSendMail(){
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            showMailError()
        }
    }
    func configureMailController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(["pomodoroapp01@gmail.com"])
        mailComposerVC.setSubject("Question about PomoApp")
        mailComposerVC.setMessageBody("Please write only in English, thanks!", isHTML: false)
        return mailComposerVC
    }
    func showMailError(){
        let sendMailerrorAlert = UIAlertController(title: "Could not send email", message:"Your device could not send email", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Ok", style: .default, handler: nil)
        sendMailerrorAlert.addAction(dismiss)
        self.present(sendMailerrorAlert, animated: true, completion: nil)
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
