//
//  SelectTimerTableViewController.swift
//  pomoapp
//
//  Created by We are Go-getters on 4/6/19.
//  Copyright © 2019 We are Go-getters. All rights reserved.
//

import UIKit

class SelectTimerTableViewController: UITableViewController {
    let pomoTimers = PomoTimers.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pomoTimers.numTimers()
    }
    
    // Cancel timer selection
    @IBAction func cancelBTN(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Configure cell labels
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        // Add timer name
        cell.textLabel?.text = pomoTimers.timeIntervals[indexPath.row].timerName
        // Add timer info
        cell.detailTextLabel?.text = "Work: \(pomoTimers.timeIntervals[indexPath.row].workLength / 60)min - Short: \(pomoTimers.timeIntervals[indexPath.row].shortBreakLength / 60)min - Long: \(pomoTimers.timeIntervals[indexPath.row].longBreakLength / 60)min - Total: \(pomoTimers.timeIntervals[indexPath.row].totalRepititions * ((pomoTimers.timeIntervals[indexPath.row].repititionsBeforeLongBreak * (pomoTimers.timeIntervals[indexPath.row].workLength / 60 + pomoTimers.timeIntervals[indexPath.row].shortBreakLength / 60)) + pomoTimers.timeIntervals[indexPath.row].longBreakLength / 60))min"
        return cell
    }
    
    // Upon selection of a timer, return to main timer screen and load that timer
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pomoTimers.selectedTimer = pomoTimers[tableView.indexPathForSelectedRow!.row].genTimeStack()
        pomoTimers.selectedTimerName = pomoTimers[tableView.indexPathForSelectedRow!.row].timerName
        self.dismiss(animated: true, completion: nil)
    }
    
    // Support swiping left to delete a timer
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            pomoTimers.delete(at: indexPath.row)
            tableView.reloadData()
        }
    }
    
    // Reload table data when view appears
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
}
